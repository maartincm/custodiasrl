Por defecto es Requerido y Char Libre
[O] Opcional
[S] Seleccion
[P] Predictivo

## Informacion Personal

#### Identificacion
* Nombre
* Apellido
* Fecha de Nacimiento
* Documento
* Cuit/Cuil [O]
* Sexo [S]
* Estado Civil [S] [Soltero, Casado, Viudo, Divorciado]
* Nacionalidad [S]

#### Direccion
* Domicilio
* Numero
* Piso [O]
* Departamento [O]
* Ciudad [S]
* Provincia [S]

#### Contacto
* Email
* Telefono Fijo
* Codigo de Area [S]
* Celular

* Otro contacto [O]
    * Nombre
    * Apellido
    * Telefono
    * Email


## Experiencias Laborales
Carga Ilimitada, permite crear registros nuevos por cada experiencia. Puede no tener experiencia laboral. Limite a 20
#### Empresa
* Nombre de la Empresa [P]
* Actividad de la Empresa [O]
* Pais
#### Posicion
* Puesto
* Nivel de Experiencia [S] [Training, Junior, SemiSenior, Senior]
* Desde [Mes Anio]
* Hasta [Mes Anio][o *al presente* en Booleano (Bloquea el Hasta)]
* Descripcion de Responsabilidades [TextArea][O]
* Personas a cargo [Int][O]
#### Referencia
* Nombre y Apellido del Referente
* Telefono [O]
* Email [O]


## Educacion
Carga Ilimitada, permite crear registros nuevos por cada educacion. Puede no Tener educacion. Limite a 10
#### Institucion
* Institucion
* Pais
#### Estudio
* Tipo de Estudio [S] [Secundario, Terciario, Universitario, Otro]
* Estado [S] [En curso, Graduado, Abandonado]
* Titulo/Carrera
* Inicio [Mes Anio]
* Fin [Mes Anio][o *al presente* en Bool (Bloquea Fin)]


## Finalizado 
#### Pretenciones Laborales
* Preferencia Salarial [O]
* Carta de Presentacion [TextArea][O]
* Disponibilidad para trabajar a mas de 60km [Bool][O]
