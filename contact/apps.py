from django.apps import AppConfig
from material.frontend.apps import ModuleMixin


class ContactConfig(ModuleMixin, AppConfig):
    name = 'contact'
    icon = '<i class="material-icons">person</i>'
