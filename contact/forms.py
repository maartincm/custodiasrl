from django.forms import ModelForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Div, HTML

from . import models


class ContactForm(ModelForm):
    class Meta:
        model = models.Contact
        fields = [
            'first_name', 'last_name'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'contact-form'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.form_action = 'personal_information'

        self.helper.template_pack = 'bootstrap4-material'

        self.col_css = 'md-form col-12 '  # form-group col-md-6 mb-0 md-form'
        self.two_col = 'col-md-6 '
        self.three_col = 'col-md-4 '

        self.helper.layout = Layout(
            Row(
                Column('first_name', css_class=self.col_css + self.two_col),
                Column('last_name', css_class=self.col_css + self.two_col),
                # css_class='form-row'
            ),
            # Row(
            #     Column('birthday', css_class=self.col_css + self.two_col),
            #     Column('identification', css_class=self.col_css + self.two_col),
            # ),
            # Row(
            #     Column('email', css_class=self.col_css),
            # ),
            # Row(
            #     Column('city', css_class=self.col_css + self.three_col),
            #     Column('state', css_class=self.col_css + self.three_col),
            #     Column('zip_code', css_class=self.col_css + self.three_col),
            #     # css_class='form-row'
            # ),
            # Row(
            #     Column('gender', css_class=self.col_css),
            # ),
            Div(Submit('submit', 'Next'), css_class="float-sm-right"),
        )


class LaboralExperiencesForm(ModelForm):
    class Meta:
        model = models.LaboralExperience
        fields = [
            'company_name',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'contact-form'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.form_action = 'laboral_experiences'

        self.helper.template_pack = 'bootstrap4-material'

        self.col_css = 'md-form col-12 '  # form-group col-md-6 mb-0 md-form'
        self.two_col = 'col-md-6 '
        self.three_col = 'col-md-4 '

        self.helper.layout = Layout(
            Row(
                Column('company_name', css_class=self.col_css),
                # css_class='form-row'
            ),
            # Row(
            #     Column('birthday', css_class=self.col_css + self.two_col),
            #     Column('identification', css_class=self.col_css + self.two_col),
            # ),
            # Row(
            #     Column('email', css_class=self.col_css),
            # ),
            # Row(
            #     Column('city', css_class=self.col_css + self.three_col),
            #     Column('state', css_class=self.col_css + self.three_col),
            #     Column('zip_code', css_class=self.col_css + self.three_col),
            #     # css_class='form-row'
            # ),
            # Row(
            #     Column('gender', css_class=self.col_css),
            # ),
            Div(
                Submit('submit', 'Add'),
                css_class="float-sm-right"),
       )
