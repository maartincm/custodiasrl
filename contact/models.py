from django.db import models
from django.utils.translation import gettext as _


class Country(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Contact(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(null=True)
    image = models.FileField(upload_to="contact/", null=True)
    city = models.CharField(max_length=255, null=True)
    state = models.ForeignKey(State, null=True, on_delete=models.SET_NULL)
    zip_code = models.CharField(max_length=255, null=True)
    birthday = models.DateField(null=True)
    identification = models.CharField(max_length=255, null=True)
    gender = models.CharField(choices=[
        ('male', _("Male")),
        ('female', _("Female"))], max_length=255, null=True)

    def __str__(self):
        return (" ").join([self.first_name, self.last_name])


class LaboralExperience(models.Model):
    contact = models.ForeignKey(Contact, null=False, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=255)

    def __str__(self):
        return (" ").join([self.company_name])
