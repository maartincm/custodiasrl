from django.contrib import admin
from django import forms

from .models import Country, State, Contact


class StateInline(admin.TabularInline):
    model = State
    fields = ('name', )

    formset = forms.models.BaseInlineFormSet


@admin.register(Country)
class Country(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
    )
    inlines = [
        StateInline,
    ]
    list_filter = ('name', )


@admin.register(State)
class State(admin.ModelAdmin):
    list_display = ('name', 'country')


@admin.register(Contact)
class Contact(admin.ModelAdmin):
    pass
