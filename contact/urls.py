from django.urls import path
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    path('', RedirectView.as_view(url='./form/personal_information'),
         name="index"),
    path('form/personal_information', views.ContactFormView.as_view(),
         name="contact-form"),
    path('form/laboral_experiences',
         views.LaboralExperiencesView.as_view(),
         name="laboral-experiences-form"),
]
