from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render

from . import forms


class ContactFormView(FormView):
    template_name = 'contact/contact_form.html'
    form_class = forms.ContactForm

    def post(self, request):
        form = self.get_form()
        new_contact = form.save()
        request.session['contact_id'] = new_contact.id
        next_url = reverse('laboral-experiences-form')
        return HttpResponseRedirect(next_url)


class LaboralExperiencesView(FormView):
    template_name = 'contact/laboral_experiences.html'
    form_class = forms.LaboralExperiencesForm

    def post(self, request):
        new_contact = request.session.get('contact_id')
        if not new_contact:
            # TODO Resubmission?
            first_url = reverse('contact-form')
            return HttpResponseRedirect(first_url)
        form = self.get_form()
        new_laboral_exp = form.save(commit=False)
        new_laboral_exp.contact_id = new_contact
        new_laboral_exp.save()
        laboral_experiences = form._meta.model.objects.filter(
            contact_id=new_contact)
        context = self.get_context_data()
        context.update({
            'laboral_experiences': laboral_experiences,
        })
        return render(request, self.template_name, context)
